(in-package :rotten-tomatoes)

(defun heroku-getenv (target)
  #+ccl (ccl:getenv target)
  #+sbcl (sb-posix:getenv target))

(defvar *database-url* (heroku-getenv "DATABASE_URL"))

(defun db-params ()
  (let* ((url (second (cl-ppcre:split "//" *database-url*)))
         (user (first (cl-ppcre:split ":" (first (cl-ppcre:split "@" url)))))
         (password (second (cl-ppcre:split ":" (first (cl-ppcre:split "@" url)))))
         (host (first (cl-ppcre:split "/" (second (cl-ppcre:split "@" url)))))
         (database (second (cl-ppcre:split "/" (second (cl-ppcre:split "@" url))))))
    (list database user password host)))
