(ql:quickload :hunchentoot)
(ql:quickload :rotten-tomatoes)

(defvar *h* (make-instance 'hunchentoot:easy-acceptor :port 3000))

(hunchentoot:start *h*)
