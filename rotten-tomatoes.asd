(asdf:defsystem #:rotten-tomatoes
  :serial t
  :description "Rotten Tomatoes with alien technology"
  :depends-on (#:hunchentoot
               #:cl-who
               #:postmodern
               #:simple-date)
  :components ((:file "package")
               (:module :src
                :serial t
                :components
                        ((:module :helpers
                          :serial t
                          :components ((:file "general")
                                       (:file "date")
                                       (:file "heroku")
                                       (:file "html")
                                       (:file "model")))
                         (:module :models
                          :serial t
                          :components ((:file "movie")))
                         (:module :views
                          :serial t
                          :components
                                  ((:module :layouts
                                    :serial t
                                    :components ((:file "application")))
                                   (:module :movies
                                    :serial t
                                    :components ((:file "index")
                                                 (:file "new")
                                                 (:file "show")
                                                 (:file "edit")))))
                         (:module :controllers
                          :serial t
                          :components ((:file "movies")))
                         ))))
