(in-package :rotten-tomatoes)

(setq *dispatch-table*
      (list
       (create-regex-dispatcher "^/$" 'controller-to-index)
       (create-regex-dispatcher "^/movies$" 'controller-index)
       (create-regex-dispatcher "^/movies/new" 'controller-new)
       (create-regex-dispatcher "^/movies/[0-9]+$" 'controller-show)
       (create-regex-dispatcher "^/movies/[0-9]+/edit" 'controller-edit)
       (create-regex-dispatcher "^/movies/[0-9]+/update" 'controller-update)
       (create-regex-dispatcher "^/movies/[0-9]+/delete" 'controller-destroy)
       (create-regex-dispatcher "^/add-movie" 'controller-add-movie)
       (create-static-file-dispatcher-and-handler "/site.css" "static/application.css")))

(defun controller-to-index ()
  (redirect "/movies"))

(defun controller-add-movie ()
  "For adding new movies."
  (let ((title (parameter "movie-title"))
        (rating (parameter "movie-rating"))
        (year (parse-integer (parameter "year")))
        (month (parse-integer (parameter "month")))
        (day (parse-integer (parameter "day"))))
    (movie-create :title title :rating rating :release-date (encode-date year month day)))
  (redirect "/movies"))

(defun controller-update ()
  (let ((title (parameter "movie-title"))
        (rating (parameter "movie-rating"))
        (year (parse-integer (parameter "year")))
        (month (parse-integer (parameter "month")))
        (day (parse-integer (parameter "day")))
        (movie (movie-get (get-id-from-uri))))
    (setf (movie-title movie) title
          (movie-rating movie) rating
          (movie-release-date movie) (encode-date year month day))
    (movie-update movie))
  (redirect (conc "/movies/" (get-id-from-uri))))
