(in-package :cl-user)

(print ">>> Building system...")

(load (make-pathname :directory *build-dir* :defaults "rotten-tomatoes.asd"))

(ql:quickload :rotten-tomatoes)

(print ">>> Done building system")
